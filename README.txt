
Managesite
==========

This module provides a way to build a control panel similar to the one provides by
Drupal on the admin zone (/admin).

You can create a number of different panels and assign them to roles.

In this panel you can create regions and insert any menu item from the menu
trees.

It also provides a hook to create custom blocks. The module provides two example
blocks.

There are also a couple of default views that are linked from one of the default
blocks that allows to do simple management tasks with contents.

This module is created as a way to provide the end user with a control panel as
simple as possible, with just the necessary links to the options they can
manage.
